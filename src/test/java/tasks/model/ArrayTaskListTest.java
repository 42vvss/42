package tasks.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTaskListTest {
    ArrayTaskList list;

    @BeforeEach
    void setUp() {
        list = new ArrayTaskList();
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void add_ECP_start_time_in_past_error() {
        Task task = new Task("a", new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 10)));
        assertThrows(RuntimeException.class, () -> list.add(task));
    }

    @Test
    void add_ECP_start_time_in_future_succes() {
        Task task = new Task("a", new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 24 * 10)));
        list.add(task);
        assertEquals("a", list.getTask(list.size() - 1).getTitle());
    }

    @Test
    void add_BVA_start_time_minutes_in_the_future_succes() {
        Task task = new Task("a", new Date(System.currentTimeMillis() + 1));
        list.add(task);
        assertEquals("a", list.getTask(list.size() - 1).getTitle());
    }

    @Test
    void add_BVA_start_time_minutes_in_the_past_error() {
        Task task = new Task("a", new Date(System.currentTimeMillis() - 1));
        assertThrows(RuntimeException.class, () -> list.add(task));
    }

    @Test
    void add_BVA_start_time_is_current_time_succes() {
        Task task = new Task("a", new Date(System.currentTimeMillis()));
        list.add(task);
        assertEquals("a", list.getTask(list.size() - 1).getTitle());
    }

    @Test
    void add_ECP_end_time_before_start_time_error() {
        long currentTime = System.currentTimeMillis() + 1;
        Task task = new Task("a", new Date(currentTime), new Date(currentTime - 1000 * 60 * 60 * 24 * 10));
        assertThrows(RuntimeException.class, () -> list.add(task));
    }

    @Test
    void add_BVA_end_time_eq_start_time_error() {
        long currentTime = System.currentTimeMillis() + 1;
        Task task = new Task("a", new Date(currentTime), new Date(currentTime));
        assertThrows(RuntimeException.class, () -> list.add(task));
    }

    @Test
    void add_ECP_start_time_before_end_time_eq_start_time_error() {
        long currentTime = System.currentTimeMillis() + 1;
        Task task = new Task("a", new Date(currentTime - 1000 * 60 * 60 * 24 * 10), new Date(currentTime - 1000 * 60 * 60 * 24 * 11));
        assertThrows(RuntimeException.class, () -> list.add(task));
    }

    @Test
    void add_BVA_end_time_after_start_time() {
        long currentTime = System.currentTimeMillis() + 1;
        Task task = new Task("a", new Date(currentTime), new Date(currentTime + 1));
        list.add(task);
        assertEquals("a", list.getTask(list.size() - 1).getTitle());
    }
}